<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'id' => 1,
                'label' => 'student',
                'name' => 'Student'
            ],
            [
                'id' => 2,
                'label' => 'employee',
                'name' => 'Employee'
            ]
        ];

        foreach ($roles as $role) {
            Role::create($role);
        }
    }
}
