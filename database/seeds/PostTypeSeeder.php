<?php

use Illuminate\Database\Seeder;

class PostTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [
                'id' => 1,
                'label' => 'status',
                'name' => 'Status'
            ],
            [
                'id' => 2,
                'label' => 'timeline-photo',
                'name' => 'Timeline Photo'
            ]
        ];

        foreach ($types as $type) {
            \App\Models\PostType::create([
                'id' => $type['id'],
                'label' => $type['label'],
                'name' => $type['name']
            ]);
        }
    }
}
