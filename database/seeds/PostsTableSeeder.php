<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        \App\User::inRandomOrder()->chunk(100, function($users) use ($faker) {
            $now = now();
            foreach ($users as $user) {
                $numberOfPosts = $faker->numberBetween(5, 500);

                $postsData = [];
                for ($i = 1; $i <= $numberOfPosts; $i++) {
                    $postsData[] = [
                        'type_id' => $faker->boolean(80) ? 1 : 2,
                        'user_id' => $user->id,
                        'created_at' => $faker->dateTimeBetween($user->created_at, $now),
                        'updated_at' => $faker->dateTimeBetween($user->created_at, $now),
                    ];
                }

                DB::table('posts')->insert($postsData);

                $statusPosts = DB::table('posts')
                    ->where('user_id', $user->id)
                    ->where('type_id', 1)
                    ->pluck('id')
                    ->toArray();

                // check existing statuses
                $existingStatuses = DB::table('statuses')
                    ->whereIn('post_id', $statusPosts)
                    ->pluck('post_id')
                    ->toArray();

                $statusPosts = array_diff($statusPosts, $existingStatuses);
                $newStatusesData = [];
                foreach ($statusPosts as $postId) {
                    $newStatusesData[] = [
                        'post_id' => $postId,
                        'content' => $faker->paragraphs(random_int(1, 4), true)
                    ];
                }

                DB::table('statuses')->insert($newStatusesData);

                $timelinePhotosPosts = DB::table('posts')
                    ->where('user_id', $user->id)
                    ->where('type_id', 2)
                    ->pluck('id')
                    ->toArray();

                // check existing statuses
                $existingPhotos = DB::table('timeline_photos')
                    ->whereIn('post_id', $timelinePhotosPosts)
                    ->pluck('post_id')
                    ->toArray();

                $timelinePhotosPosts = array_diff($timelinePhotosPosts, $existingPhotos);
                $newTimelinePhotosData = [];
                foreach ($timelinePhotosPosts as $postId) {
                    $newTimelinePhotosData[] = [
                        'post_id' => $postId,
                        'description' => $faker->optional(0.85)
                            ->paragraphs(random_int(1, 2), true)
                    ];
                }

                DB::table('timeline_photos')->insert($newTimelinePhotosData);
            }
        });
    }
}
