<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Ivan Bernatović',
            'email' => 'ivan.bernatovic.93@gmail.com',
            'password' => bcrypt('password')
        ]);

        factory(App\User::class, 500)->create();
    }
}
