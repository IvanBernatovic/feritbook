<?php

use Illuminate\Database\Seeder;

class LikesAndCommentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $now = now();
        \App\User::select(['id'])->chunk(100, function ($users) use ($faker, $now) {
            foreach ($users as $user) {
                /* @var \App\User $user */
                $friendIds = $user->getFriendsIds();
                $user->posts()->select(['id', 'created_at'])->chunk(100, function ($posts) use ($friendIds, $faker, $now) {
                    foreach ($posts as $post) {
                        /* @var \App\Models\Post $post */
                        $numberOfLikes = $faker->biasedNumberBetween(0, (int)(count($friendIds) / 2),
                            '\Faker\Provider\Biased::linearLow');
                        $numberOfComments = $faker->biasedNumberBetween(0, $numberOfLikes, '\Faker\Provider\Biased::linearLow');

                        $likers = $faker->randomElements($friendIds, $numberOfLikes);
                        $likesData = [];
                        foreach ($likers as $userId) {
                            $likesData[] = [
                                'post_id' => $post->id,
                                'user_id' => $userId,
                                'created_at' => $faker->dateTimeBetween($post->created_at, $now)
                            ];
                        }

                        DB::table('likes')->insert($likesData);

                        $commenters = $faker->randomElements($friendIds, $numberOfComments);
                        $commentData = [];
                        foreach ($commenters as $userId) {
                            $commentData[] = [
                                'post_id' => $post->id,
                                'user_id' => $userId,
                                'content' => $faker->paragraphs(random_int(1, 2), true),
                                'created_at' => $faker->dateTimeBetween($post->created_at, $now)
                            ];
                        }

                        DB::table('comments')->insert($commentData);
                    }
                });
            }
        });
    }
}
