<?php

use Illuminate\Database\Seeder;

class FriendshipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        \App\User::chunk(100, function($users) use ($faker) {
           foreach ($users as $user) {
               $receiverIds = DB::table('friendships')
                   ->where('sender_id', $user->id)
                   ->whereNull('deleted_at')
                   ->pluck('receiver_id')
                   ->toArray();

               $senderIds = DB::table('friendships')
                   ->where('receiver_id', $user->id)
                   ->whereNull('deleted_at')
                   ->pluck('sender_id')
                   ->toArray();

               $currentFriendsIds = array_unique(array_merge($receiverIds, $senderIds));

               if (count($currentFriendsIds) > 130) {
                   continue;
               }

               // get random users that are not friends yet
               $randomUsersIds = \App\User::whereNotIn('id', array_merge($currentFriendsIds, [$user->id]))
                   ->inRandomOrder()
                   ->limit($faker->biasedNumberBetween(3, 250, '\Faker\Provider\Biased::linearLow'))
                   ->pluck('id')
                   ->toArray();


               $now = now()->toDateTimeString();
               $newFriendshipsData = [];
               foreach ($randomUsersIds as $friendId) {
                   if ($faker->boolean()) {
                       $senderId = $user->id;
                       $receiverId = $friendId;
                   } else {
                       $senderId = $friendId;
                       $receiverId = $user->id;
                   }

                   $newFriendshipsData[] = [
                       'sender_id' => $senderId,
                       'receiver_id' => $receiverId,
                       'accepted' => $faker->optional(0.97)->boolean(95),
                       'created_at' => $now,
                       'updated_at' => $now
                   ];
               }

               DB::table('friendships')->insert($newFriendshipsData);
           }
        });
    }
}
