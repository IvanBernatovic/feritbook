<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/search', 'SearchController@search')->name('search');
Route::post('/status', 'StatusController@store')->name('status.store');
Route::post('/timeline-photo', 'TimelinePhotoController@store')->name('timeline-photo.store');
Route::get('/profile/{user}', 'ProfileController@show')->name('profile.show');

Route::post('user/{receiver}/friendship/add', 'FriendshipController@addFriend')->name('friendship.add');
Route::post('/friendship/{friendship}/accept', 'FriendshipController@acceptFriend')->name('friendship.accept');
Route::post('/friendship/{friendship}/refuse', 'FriendshipController@refuseFriend')->name('friendship.refuse');
Route::post('/friendship/{friendship}/remove', 'FriendshipController@removeFriend')->name('friendship.remove');
Route::get('/friends', 'ProfileController@friends')->name('friends');
Route::get('/settings', 'SettingsController@edit')->name('profile.settings');
Route::put('/settings', 'SettingsController@update')->name('profile.settings.update');

Route::post('/post/{post}/comments', 'CommentController@store')->name('post.comments.store');
Route::post('/post/{post}/likes', 'LikeController@like')->name('post.like');
Route::delete('/post/{post}/likes', 'LikeController@unlike')->name('post.unlike');