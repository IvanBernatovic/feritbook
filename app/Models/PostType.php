<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostType extends Model
{
    const STATUS = 1;
    const TIMELINE_PHOTO = 2;

    protected $fillable = ['label'];
}
