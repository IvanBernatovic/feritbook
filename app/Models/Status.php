<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = ['content', 'post_id'];

    /*
     * Accessors and mutators
     */

    public function getContentAttribute($content)
    {
        return nl2br($content);
    }
}
