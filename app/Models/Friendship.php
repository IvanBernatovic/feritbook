<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Friendship extends Model
{
    use SoftDeletes;

    protected $fillable = ['sender_id', 'receiver_id', 'accepted'];

    protected $casts = [
        'accepted' => 'boolean'
    ];

    protected $dates = ['deleted_at'];

    /*
     * Relationships
     */

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_id');
    }

    /*
     * Scopes
     */

    public function scopeAccepted($query)
    {
        return $query->where('accepted', true);
    }

    public function scopePending($query)
    {
        return $query->whereNull('accepted');
    }

    public function scopeConnected($query, $sender, $receiver)
    {
        return $query->whereIn('sender_id', [$sender->id, $receiver->id])
            ->whereIn('receiver_id', [$sender->id, $receiver->id]);
    }

    /*
     * Methods
     */

    /**
     * @return mixed
     */
    public function getFriendAttribute($user = null)
    {
        if (\Auth::user()->id !== $this->sender_id) {
            return $this->sender;
        }

        return $this->receiver;
    }
}
