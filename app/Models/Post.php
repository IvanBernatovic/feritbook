<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['user_id', 'type_id'];


    /*
     * Relationships
     */

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function type()
    {
        return $this->belongsTo(PostType::class, 'type_id', 'id');
    }

    public function status()
    {
        return $this->hasOne(Status::class);
    }

    public function timeline_photo()
    {
        return $this->hasOne(TimelinePhoto::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    public function subType()
    {

    }

    public function likesCount()
    {
        return $this->hasOne(Like::class)
            ->selectRaw('post_id, count(*) as count')
            ->groupBy('post_id');
    }

    /*
     * Accessors and mutators
     */

    /*
     * Scopes
     */

    public function scopeForFeed($query)
    {
        return $query->with(['likesCount',
            'status' => function ($query) {
                return $query->select(['id', 'post_id', 'content']);
            }, 'timeline_photo' => function ($query) {
                return $query->select(['id', 'post_id', 'description', 'name']);
            }, 'user' => function ($query) {
                return $query->select(['id', 'name', 'profile_photo']);
            }, 'comments' => function ($query) {
                return $query->latest()
                    ->select(['id', 'content', 'user_id', 'post_id', 'created_at'])
                    ->with(['user' => function($query) {
                    return $query->select(['id', 'name', 'profile_photo']);
                }]);
            }]);
    }

    /**
     * @param $posts
     * @param $postsLikedByUser
     *
     * Appends required additional information to posts feed
     */
    public static function appendToFeed($posts, $postsLikedByUser)
    {
        $posts->each(function ($post) use ($postsLikedByUser) {
            $post->is_liked_by_user = in_array($post->id, $postsLikedByUser);
            $post->likes_count = $post->likesCount ? $post->likesCount->count : 0;
        });
    }
}
