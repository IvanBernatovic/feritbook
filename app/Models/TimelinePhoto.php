<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TimelinePhoto extends Model
{
    protected $fillable = ['description', 'name', 'post_id'];

    /*
     * Accessors and mutators
     */

    public function getDescriptionAttribute($description)
    {
        return nl2br($description);
    }

    public function getMediumPathAttribute()
    {
        if ($this->name) {
            return \Storage::disk('public')->url('/timeline-photos/medium/' . $this->name);
        }

        $randomImage = random_int(0, 600);
        return "https://picsum.photos/800/540/?image={$randomImage}";
    }
}
