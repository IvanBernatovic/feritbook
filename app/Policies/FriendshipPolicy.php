<?php

namespace App\Policies;

use App\Models\Friendship;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class FriendshipPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function addFriend($sender, $receiver)
    {
        return !Friendship::where('sender_id', $sender->id)
            ->where('receiver_id', $receiver->id)
            ->where('accepted', true)
            ->count();
    }
}
