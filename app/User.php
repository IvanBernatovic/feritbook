<?php

namespace App;

use App\Models\Friendship;
use App\Models\Post;
use App\Models\Role;
use DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Sofa\Eloquence\Eloquence;

class User extends Authenticatable
{
    use Notifiable, Eloquence;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'description', 'profile_photo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $searchableColumns = ['name'];

    /*
     * Relationships
     */

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function all_friendships()
    {
        return Friendship::where('sender_id', $this->id)
            ->orWhere('receiver_id', $this->id);
    }

    public function friendships()
    {
        return Friendship::where('sender_id', $this->id)
            ->orWhere('receiver_id', $this->id)
            ->accepted()
            ->with(['sender', 'receiver']);
    }

    public function getFriendsIds()
    {
        $receiverIds = DB::table('friendships')
            ->where('sender_id', $this->id)
            ->whereNull('deleted_at')
            ->where('accepted', 1)
            ->pluck('receiver_id')
            ->toArray();

        $senderIds = DB::table('friendships')
            ->where('receiver_id', $this->id)
            ->whereNull('deleted_at')
            ->where('accepted', 1)
            ->pluck('sender_id')
            ->toArray();

        return array_unique(array_merge($receiverIds, $senderIds));
    }

    public function getMyFriendRequests()
    {
        return Friendship::where('receiver_id', $this->id)
            ->pending()
            ->with(['sender', 'receiver'])
            ->get();
    }

    public function getMyFriendRequestsCount()
    {
        return Friendship::where('receiver_id', $this->id)
            ->pending()
            ->with(['sender', 'receiver'])
            ->count();
    }

    /**
     * @return User
     */
    public function friends()
    {
        $receivers = DB::table('friendships')
            ->where('accepted', 1)
            ->where('sender_id', $this->id)
            ->whereNull('deleted_at')
            ->pluck('receiver_id')
            ->toArray();

        $senders = DB::table('friendships')
            ->where('accepted', 1)
            ->where('receiver_id', $this->id)
            ->whereNull('deleted_at')
            ->pluck('sender_id')
            ->toArray();

        $friendsIds = array_merge($receivers, $senders);

        return User::whereIn('id', $friendsIds)
            ->orderBy('name', 'ASC');
    }

    /*
     * Methods
     */

    /**
     * @param User $otherUser
     * @return Friendship|null
     */
    public function isFriendWith($otherUser)
    {
        return Friendship::accepted()
            ->connected(\Auth::user(), $otherUser)
            ->first();
    }

    /**
     * @param User $otherUser
     * @return Friendship|null
     */
    public function hasPendingFriendRequestWith($otherUser)
    {
        return Friendship::pending()
            ->connected(\Auth::user(), $otherUser)
            ->first();
    }

    /*
     * Accessors and mutators
     */

    public function getFriendshipsAttribute()
    {
        return $this->friendships()->get();
    }

    public function getProfilePhotoThumbnailPathAttribute()
    {
        if ($this->profile_photo) {
            return \Storage::disk('public')->url('/img/thumbnail/' . $this->profile_photo);
        }

        return "https://api.adorable.io/avatars/100/{$this->email}";
    }

    public function getProfilePhotoMediumPathAttribute()
    {
        if ($this->profile_photo) {
            return \Storage::disk('public')->url('/img/medium/' . $this->profile_photo);
        }

        return "https://api.adorable.io/avatars/200/{$this->email}";
    }
}
