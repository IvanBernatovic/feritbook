<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // Using Closure based composers...
        View::composer('layouts.app', function ($view) {
            if (\Auth::user()) {
                $view->with('friendRequestsCount', \Auth::user()->getMyFriendRequestsCount());
            }
        });
    }
}
