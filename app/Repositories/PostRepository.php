<?php

namespace App\Repositories;


use App\Models\Comment;
use App\Models\Post;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class PostRepository
{
    /**
     * @param $data
     * @return Post|Model
     */
    public function create($data)
    {
        return Auth::user()->posts()->create($data);
    }

    /**
     * @param $post
     * @param $commentData
     * @return Comment|Model
     */
    public function addComment($post, $commentData)
    {
        $commentData['user_id'] = Auth::user()->id;

        return $post->comments()->create($commentData)->refresh();
    }

    public function likePost($post)
    {
        return $post->likes()->create([
            'user_id' => Auth::user()->id
        ]);
    }

    public function unlikePost($post)
    {
        return $post->likes()->where('user_id', Auth::user()->id)->delete();
    }
}