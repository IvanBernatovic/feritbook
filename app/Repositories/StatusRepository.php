<?php

namespace App\Repositories;

use App\Models\PostType;
use App\Models\Status;

class StatusRepository
{
    public $postRepo;

    /**
     * StatusRepository constructor.
     * @param PostRepository $postRepo
     */
    public function __construct(PostRepository $postRepo)
    {
        $this->postRepo = $postRepo;
    }

    /**
     * @param $data
     * @return Status
     * @throws \Throwable
     */
    public function store($data)
    {
        return \DB::transaction(function () use ($data) {
            $post = $this->postRepo->create(['type_id' => PostType::STATUS]);
            return $post->status()->create([
                'content' => array_get($data, 'content')
            ]);
        });
    }
}