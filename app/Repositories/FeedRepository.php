<?php

namespace App\Repositories;


use App\Models\Post;
use App\User;

class FeedRepository
{
    /**
     * @param User $user
     * @param int $page
     * @return void
     */
    public function getFeedForUser($user, $page = 1)
    {
        $posts = \Cache::remember("user-{$user->id}-feed", null, function () use ($user, $page) {
            $friendsIds = $user->getFriendsIds();

            $postsLikedByUser = \DB::table('likes')
                ->where('user_id', $user->id)
                ->pluck('post_id')
                ->toArray();

            $posts = Post::whereIn('user_id', $friendsIds)
                ->latest()
                ->forFeed();


            $posts = $posts->paginate(30, ['id', 'user_id', 'type_id', 'created_at'], 'page', $page);
            Post::appendToFeed($posts, $postsLikedByUser);

            return $posts;
        });

        return $posts;
    }

    public function getFeedForProfile($user, $profile, $page = 1)
    {
        $postsLikedByUser = \DB::table('likes')
            ->where('user_id', $user->id)
            ->pluck('post_id')
            ->toArray();

        $posts = Post::where('user_id', $profile->id)
            ->latest()
            ->forFeed();


        $posts = $posts->paginate(30, ['id', 'user_id', 'type_id', 'created_at'], 'page', $page);
        Post::appendToFeed($posts, $postsLikedByUser);

        return $posts;
    }

}