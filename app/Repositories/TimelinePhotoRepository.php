<?php

namespace App\Repositories;

use App\Models\PostType;
use App\Models\Status;
use Illuminate\Http\UploadedFile;

class TimelinePhotoRepository
{
    public $postRepo;

    /**
     * StatusRepository constructor.
     * @param PostRepository $postRepo
     */
    public function __construct(PostRepository $postRepo)
    {
        $this->postRepo = $postRepo;
    }

    /**
     * @param $data
     * @param UploadedFile $image
     * @return Status
     * @throws \Throwable
     */
    public function store($data, $image)
    {
        return \DB::transaction(function () use ($data, $image) {

            $post = $this->postRepo->create(['type_id' => PostType::TIMELINE_PHOTO]);
            $name = $this->processTimelinePhoto($image);

            return $post->timeline_photo()->create([
                'description' => array_get($data, 'description'),
                'name' => $name
            ]);
        });
    }

    /**
     * @param $image
     * @return string
     */
    protected function processTimelinePhoto($image)
    {
        $name = str_random() . ".jpg";
        $medium = \Image::make($image)->resize(700, 700, function($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->stream('jpg');

        \Storage::disk('public')->put("/timeline-photos/medium/$name", $medium);

        $large = \Image::make($image)->resize(2200, 2200, function($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->stream('jpg');

        \Storage::disk('public')->put("/timeline-photos/large/$name", $large);

        return $name;
    }
}