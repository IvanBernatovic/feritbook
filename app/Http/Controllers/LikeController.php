<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Repositories\PostRepository;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    public $postRepo;

    /**
     * LikeController constructor.
     * @param PostRepository $postRepo
     */
    public function __construct(PostRepository $postRepo)
    {
        $this->postRepo = $postRepo;
    }

    public function like(Request $request, Post $post)
    {
        $this->postRepo->likePost($post);

        return response()->json();
    }

    public function unlike(Request $request, Post $post)
    {
        $this->postRepo->unlikePost($post);

        return response()->json();
    }
}
