<?php

namespace App\Http\Controllers;

use App\Repositories\FeedRepository;
use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public $feedRepo;

    public function __construct(FeedRepository $feedRepo)
    {
        $this->feedRepo = $feedRepo;
    }

    public function show(User $user)
    {
        $posts = $this->feedRepo->getFeedForProfile(\Auth::user(), $user, request()->get('page') ?? 1);

        return view('profile.show', [
            'user' => $user,
            'posts' => $posts
        ]);
    }

    public function friends()
    {
        $pendingRequests = \Auth::user()->getMyFriendRequests();
        $friends = \Auth::user()
            ->friends()->paginate(30);

        return view('profile.friends', [
            'friends' => $friends,
            'pendingRequests' => $pendingRequests
        ]);
    }
}
