<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $this->validate($request, [
            'q' => ['required', 'min:1']
        ]);

        $query = $request->get('q');

        $users = User::search($query)
            ->where('id', '<>', auth()->user()->id)
            ->paginate(20);

        return view('search', [
            'users' => $users
        ]);
    }
}
