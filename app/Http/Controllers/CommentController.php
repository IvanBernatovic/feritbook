<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Repositories\PostRepository;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public $postRepo;

    /**
     * CommentController constructor.
     * @param PostRepository $postRepo
     */
    public function __construct(PostRepository $postRepo)
    {
        $this->postRepo = $postRepo;
    }

    public function store(Request $request, Post $post)
    {
        $this->validate($request, [
            'body' => 'required'
        ]);

        $this->postRepo->addComment($post, [
            'content' => $request->get('body')
        ]);

        return redirect()->back();
    }
}
