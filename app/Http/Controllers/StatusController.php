<?php

namespace App\Http\Controllers;

use App\Repositories\StatusRepository;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    public $statusRepo;

    /**
     * StatusRepository constructor.
     * @param StatusRepository $statusRepo
     */
    public function __construct(StatusRepository $statusRepo)
    {
        $this->statusRepo = $statusRepo;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'status-content' => ['required'],
        ]);

        try {
            $status = $this->statusRepo->store([
                'content' => $request->get('status-content')
            ]);

            return redirect()
                ->route('home');
        } catch (\Throwable $e) {
            throw $e;
            Log::error("Failed to create new status because: {$e->getMessage()}", $e->getTrace());

            return redirect()
                ->route('home')
                ->with('error-notification', 'Unable to create new status. Please try again.');
        }
    }
}
