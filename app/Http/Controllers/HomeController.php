<?php

namespace App\Http\Controllers;

use App\Repositories\FeedRepository;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public $feedRepository;

    /**
     * Create a new controller instance.
     *
     * @param FeedRepository $feedRepository
     */
    public function __construct(FeedRepository $feedRepository)
    {
        $this->middleware('auth');
        $this->feedRepository = $feedRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feedPosts = $this->feedRepository->getFeedForUser(Auth::user(), request()->get('page') ?? 1);

        return view('home', [
            'posts' => $feedPosts
        ]);
    }
}
