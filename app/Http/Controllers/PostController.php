<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\PostType;
use App\Repositories\PostRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PostController extends Controller
{
    public $postRepo;

    /**
     * PostController constructor.
     * @param PostRepository $postRepo
     */
    public function __construct(PostRepository $postRepo)
    {
        $this->postRepo = $postRepo;
    }

    public function storeStatus(Request $request)
    {
        $this->validate($request, [
            'status-content' => ['required'],
        ]);

        try {
            $post = $this->postRepo->create(['type_id' => PostType::STATUS]);
            $status = $post->status()->create([
                'content' => $request->get('status-content')
            ]);

            return redirect()
                ->route('home');
        } catch (\Throwable $e) {
            throw $e;
            Log::error("Failed to create new status because: {$e->getMessage()}", $e->getTrace());

            return redirect()
                ->route('home')
                ->with('error-notification', 'Unable to create new status. Please try again.');
        }
    }
}
