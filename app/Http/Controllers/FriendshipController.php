<?php

namespace App\Http\Controllers;

use App\Models\Friendship;
use App\User;
use Illuminate\Http\Request;

class FriendshipController extends Controller
{
    /**
     * @param Request $request
     * @param User $receiver
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addFriend(Request $request, User $receiver)
    {
        // $this->authorize('friendship.add', $receiver);

        Friendship::create([
            'sender_id' => \Auth::user()->id,
            'receiver_id' => $receiver->id
        ]);

        return back();
    }

    public function acceptFriend(Request $request, Friendship $friendship)
    {
        $friendship->update([
            'accepted' => true
        ]);

        return redirect()->route('profile.show', $friendship->sender);
    }

    public function refuseFriend(Request $request, Friendship $friendship)
    {
        $friendship->update([
            'accepted' => false
        ]);

        return redirect()->route('home');
    }

    public function removeFriend(Request $request, Friendship $friendship)
    {
        $friendship->delete();

        return redirect()->route('home');
    }
}
