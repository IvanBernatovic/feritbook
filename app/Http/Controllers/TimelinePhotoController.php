<?php

namespace App\Http\Controllers;

use App\Repositories\TimelinePhotoRepository;
use Illuminate\Http\Request;
use Log;

class TimelinePhotoController extends Controller
{
    public $timelinePhotosRepo;

    public function __construct(TimelinePhotoRepository $timelinePhotosRepo)
    {
        $this->timelinePhotosRepo = $timelinePhotosRepo;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'timeline_photo' => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg']
        ]);

        $data = $request->only('description');
        $image = $request->file('timeline_photo');

        try {
            $this->timelinePhotosRepo->store($data, $image);

            return redirect()
                ->route('home');
        } catch (\Throwable $e) {
            throw $e;
            Log::error("Failed to create new status because: {$e->getMessage()}", $e->getTrace());

            return redirect()
                ->route('home')
                ->with('error-notification', 'Unable to create new status. Please try again.');
        }
    }
}
