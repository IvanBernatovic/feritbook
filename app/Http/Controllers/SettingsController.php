<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class SettingsController extends Controller
{
    public function edit()
    {
        return view('profile.settings', [
            'user' => \Auth::user()
        ]);
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'role_id' => ['required', 'integer', 'exists:roles,id'],
            'description' => ['required'],
            'file' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);

        $data = $request->only(['name', 'role_id', 'description']);

        if ($request->has('profile_photo')) {
            $imageName = $this->processProfileImage($request->file('profile_photo'));
            $data['profile_photo'] = $imageName;
        }

        \Auth::user()->update($data);

        return redirect()->route('profile.show', \Auth::user());
    }

    /**
     * @param UploadedFile $imageFile
     * @return string
     */
    protected function processProfileImage($imageFile)
    {
        $name = str_random() . ".jpg";

        $thumbnail = \Image::make($imageFile)->resize(150, 150, function($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->stream('jpg');

        \Storage::disk('public')->put("/img/thumbnail/$name", $thumbnail);

        $medium = \Image::make($imageFile)->resize(1000, 1000, function($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->stream('jpg');

        \Storage::disk('public')->put("/img/medium/$name", $medium);

        $large = \Image::make($imageFile)->resize(2200, 2200, function($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        })->stream('jpg');

        \Storage::disk('public')->put("/img/large/$name", $large);

        return $name;
    }
}
