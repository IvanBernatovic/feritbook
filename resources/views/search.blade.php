@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6">
        @if ($count = $users->total())
          <h3>People ({{ $count }})</h3>
          @foreach($users as $user)
            @include('partials.user-card', ['user' => $user])
          @endforeach

          <div class="mt-5 row d-flex justify-content-center">
            {{ $users->appends(['q' => request()->get('q')])->links() }}
          </div>
        @else
          <h3>No users for search "{{ request()->get('q') }}"</h3>
        @endif
      </div>
    </div>
  </div>
@endsection
