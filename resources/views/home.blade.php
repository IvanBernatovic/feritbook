@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">

      @if (session('error-notification'))
        <div class="col-md-6">
          <div class="alert alert-danger" role="alert">
            {{ session('error-notification') }}
          </div>
        </div>
      @endif

      <div class="col-md-6 offset-md-3">
        @include('partials.new-post')
      </div>

      @foreach($posts as $post)
        <div class="col-md-6 offset-md-3">
          @include('partials.post-card')
        </div>
      @endforeach
    </div>
  </div>
@endsection
