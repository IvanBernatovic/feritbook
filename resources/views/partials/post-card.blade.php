@if ($post->type_id === \App\Models\PostType::STATUS)
  <div class="post-wrapper">
    <div class="card">
      <div class="card-body pb-2">
        <div class="d-flex mb-2">
          <div style="max-width: 65px; max-height: 65px">
            <img src="{{ $post->user->profile_photo_thumbnail_path }}"
                 class="img-fluid rounded"
                 alt="{{ $post->user->name }}'s profile photo">
          </div>
          <div class="f-flex flex-column ml-2 mt-1">
            <h5 class="mb-1"><a href="{{ route('profile.show', $post->user) }}"
                                class="profile-name">{{ $post->user->name }}</a></h5>
            <small class="text-muted">{{ $post->created_at->diffForHumans() }}</small>
          </div>
        </div>
        <div class="status-content">
          {!! $post->status->content !!}
        </div>
      </div>
      @include('partials.post.actions')
      @include('partials.post.comment-section')
    </div>
  </div>
@elseif ($post->type_id === \App\Models\PostType::TIMELINE_PHOTO)
  <div class="post-wrapper">
    <div class="card">
      <div class="card-body pb-3">
        <div class="d-flex mb-2">
          <div style="max-width: 65px; max-height: 65px">
            <img src="{{ $post->user->profile_photo_thumbnail_path }}"
                 class="img-fluid rounded"
                 alt="{{ $post->user->name }}'s profile photo">
          </div>
          <div class="f-flex flex-column ml-2 mt-1">
            <h5 class="mb-1"><a href="{{ route('profile.show', $post->user) }}"
                                class="profile-name">{{ $post->user->name }}</a></h5>
            <small class="text-muted">{{ $post->created_at->diffForHumans() }}</small>
          </div>
        </div>

        <div class="status-content">
          {!! $post->timeline_photo->description !!}
        </div>

        <div class="pt-2">
          <img src="{{ $post->timeline_photo->medium_path }}" alt="" class="img-fluid">
        </div>
      </div>
      @include('partials.post.actions')
      @include('partials.post.comment-section')
    </div>
  </div>
@endif