<div class="user-card-wrapper">
  <div class="card">
    <div class="inner-wrapper d-flex">
      <div class="w-25 p-1">
        <img src="{{ $user->profile_photo_thumbnail_path }}" alt="" class="img-fluid">
      </div>
      <div class="right p-2 d-flex flex-column justify-content-between flex-wrap">
        <h5><a class="profile-name" href="{{ route('profile.show', $user) }}">{{ $user->name }}</a></h5>
        @if ($friendship = Auth::user()->isFriendWith($user))
          <form action="{{ route('friendship.remove', $friendship) }}" method="POST">
            {{ csrf_field() }}
            <input type="submit" class="btn btn-outline-danger btn-sm" value="Remove friend">
          </form>
        @elseif ($friendship = Auth::user()->hasPendingFriendRequestWith($user))
          @if ($friendship->sender_id === Auth::user()->id)
            <button class="btn btn-outline-secondary btn-sm" disabled aria-disabled="true">Friend request sent</button>
          @else
            <div class="d-flex flex-row justify-content-start">
              <form action="{{ route('friendship.accept', $friendship) }}" method="POST">
                {{ csrf_field() }}
                <input type="submit" class="btn btn-outline-success btn-sm" value="Accept friend">
              </form>
              <form class="ml-2" action="{{ route('friendship.refuse', $friendship) }}" method="POST">
                {{ csrf_field() }}
                <input type="submit" class="btn btn-outline-dark btn-sm" value="Refuse friend">
              </form>
            </div>
          @endif
        @else
          <form action="{{ route('friendship.add', $user) }}" method="POST">
            {{ csrf_field() }}
            <input type="submit" class="btn btn-outline-primary btn-sm" value="Add friend">
          </form>
        @endif
      </div>
    </div>
  </div>
</div>