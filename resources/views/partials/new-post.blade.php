<div class="card mb-3">
  <div class="card-header">Post new photo</div>

  <div class="card-body">
    {{ Form::open(['route' => 'timeline-photo.store', 'method' => 'POST', 'files' => true ]) }}
    <div class="form-group">
      <label for="timeline_photo">Pick a photo</label>
      {{ Form::file('timeline_photo', ['class' => 'form-control-file', 'required', 'id' => 'timeline_photo']) }}
    </div>

    <div class="form-group">
      <textarea name="description" id="description" rows="3" class="form-control"
                placeholder="What's on your photo"></textarea>
    </div>

    <input type="submit" name="Submit" class="btn btn-primary">
    {{ Form::close() }}
  </div>
</div>

<div class="card mb-3">
  <div class="card-header">Post new status</div>

  <div class="card-body">
    <form action="{{ route('status.store') }}" method="POST">
      {{ csrf_field() }}
      <div class="form-group">
        <textarea name="status-content" id="new-status-textarea" rows="3" class="form-control"
                  placeholder="What's on your mind"></textarea>
      </div>

      <input type="submit" name="Submit" class="btn btn-primary">
    </form>
  </div>
</div>