<div class="post-actions-wrapper px-3 pb-1 pt-0">
  <!--suppress CheckEmptyScriptTag -->
  <post-actions :count="{{ $post->likes_count }}" @if($post->is_liked_by_user) is-liked @endif :post-id="{{ $post->id }}"/>
</div>