<div class="comments-wrapper px-3 pb-3 pt-0">
  <div class="comments-list">
    @foreach($post->comments as $comment)
      <div class="comment p-2 d-flex" id="c-{{ $comment->id }}">
        <div class="comment-img-wrapper mr-2" style="">
          <img src="{{ $comment->user->profile_photo_thumbnail_path }}" alt="" class="img-max">
        </div>
        <div class="content">
          <div class="body">
            <a href="{{ route('profile.show', $comment->user) }}" class="profile-name">{{ $comment->user->name }}</a>
            &nbsp;{!! $comment->content !!}
          </div>
          <small class="text-muted">{{ $comment->created_at->diffForHumans() }}</small>
        </div>
      </div>
    @endforeach
  </div>

  {{ Form::open(['route' => ['post.comments.store', $post], 'method' => 'POST']) }}
  <div class="mb-2">
    {{ Form::textarea('body', null, ['class' => 'form-control', 'required', 'rows' => 2,
  'placeholder' => 'Write a comment...', 'id' => "p-{$post->id}-comment-ta"]) }}
  </div>

  <div>
    {{ Form::submit('Submit', ['class' => 'btn btn-primary btn-sm']) }}
  </div>

  {{ Form::close() }}
</div>