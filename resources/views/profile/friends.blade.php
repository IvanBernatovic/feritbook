@extends('layouts.app')

@section('content')
  <div class="container">
    @if($pendingCount = $pendingRequests->count())
      <div class="row justify-content-center">
        <div class="col-md-6">
          <h3>Friend requests ({{ $pendingCount }})</h3>
          @foreach($pendingRequests as $friendship)
            @include('partials.user-card', ['user' => $friendship->friend])
          @endforeach
        </div>
      </div>
    @endif
    @if($friendsCount = $friends->total())
      <div class="row justify-content-center mt-4">
        <div class="col-md-6">
          <h3>Your friends ({{ $friendsCount }})</h3>
          @foreach($friends as $friend)
            @include('partials.user-card', ['user' => $friend])
          @endforeach

          <div class="mt-5 row d-flex justify-content-center">
            {{ $friends->appends(['q' => request()->get('q')])->links() }}
          </div>
        </div>
      </div>
    @endif
  </div>
@endsection
