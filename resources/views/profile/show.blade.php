@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="card">
          <img src="{{ $user->profile_photo_medium_path }}" alt="{{ $user->name }}'s profile photo"
               class="card-img-top">
          <div class="card-body">
            <h5 class="card-title profile-name mb-0">{{ $user->name }}</h5>
            <small class="text-muted">{{ $user->role->name }}</small>
            @if ($user->description)
              <div class="card-text">
                {{ $user->description }}
              </div>
            @endif

            @if (Auth::user() && Auth::user()->id === $user->id)
              <div class="mt-2">
                <a href="{{ route('profile.settings') }}" class="btn btn-outline-info">Edit profile</a>
              </div>
            @endif
          </div>
        </div>
      </div>

      <div class="col-md-6">
        @if(Auth::user() && Auth::user()->id === $user->id)
          @include('partials.new-post')
        @endif

        @foreach($posts as $post)
            @include('partials.post-card')
        @endforeach
      </div>
    </div>
  </div>
@endsection
