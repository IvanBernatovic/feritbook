@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-6 offset-md-3">
        <div class="card">
          <div class="card-body">
            {!! Form::model($user, ['route' => 'profile.settings.update', 'method' => 'PUT', 'files' => true]) !!}
            <div class="form-group row">
              <label for="name" class="col-sm-3 col-form-label">Name</label>
              <div class="col-sm-9">
                {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'name', 'required', 'id' => 'name']) }}
              </div>
            </div>
            <div class="form-group row">
              <label for="role_id" class="col-sm-3 col-form-label">Role</label>
              <div class="col-sm-9">
                {{ Form::select('role_id', \App\Models\Role::pluck('name', 'id')->toArray(), null,
                  ['class' => 'form-control', 'required', 'id' => 'role_id']) }}
              </div>
            </div>

            <div class="form-group row">
              <label for="description" class="col-sm-3 col-form-label">Description</label>
              <div class="col-sm-9">
                {{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description', 'required',
                  'id' => 'description', 'rows' => 3]) }}
              </div>
            </div>

            <div class="form-group row">
              <label for="profile_photo" class="col-sm-3 col-form-label">Profile image</label>
              <div class="col-sm-9">
                {{ Form::file('profile_photo', ['class' => 'form-control-file', 'id' => 'profile_photo']) }}
              </div>
            </div>

            <div class="form-group row">
              <div class="col-sm-12">
                <button type="submit" class="btn btn-outline-primary">Submit</button>
                <a href="{{ route('profile.show', $user) }}" class="btn btn-outline-danger">Cancel</a>
              </div>
            </div>
            {{ Form::close() }}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
